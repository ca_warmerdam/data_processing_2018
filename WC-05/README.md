# Questions from WC-05

- I have tried to get an idea about what is available on threads, cores, sockets and CPU's
- I have drawn a picture about my own binmachine with sockets, processor and cores like this
- I have 4 threads available on my machine

![alt text](drawing.png "drawing")
![alt text](usage.png "usage")

- The program doesn't return an error
- There are 24 threads and 6 cores per socket in idefix
- The machine will try to switch between processes
- You can limit the recources with the recource keyword and specifying mem_mb

- To minimize the waiting time for 2 dependent steps
- He should add more comments
