INPUT_DIR = "data/"
OUTPUT_DIR = "out/"
FASTQ_DIR = "samples/"

rule bwa_map:
    input:
        join(INPUT_DIR, "genome.fa"),
        join(INPUT_DIR, FASTQ_DIR, "{sample}.fastq")
    output:
        join(OUTPUT_DIR, "mapped_reads/{sample}.bam")
    log:
        "logs/bwa_mem/{sample}.log"
    benchmark:
        "benchmarks/{sample}.bwa.benchmark.txt"
    threads: 32
    shell:
        "bwa mem -t {threads} {input} | samtools view -Sb - > {output}"

#rule bwa_mem:
#    input:
#        join(INPUT_DIR, "genome.fa"),
#        join(INPUT_DIR, FASTQ_DIR, "{sample}.fastq")
#    output:
#        join(OUTPUT_DIR, "mapped_reads/{sample}.bam")
#    message: "executing bwa mem on the following {input} to generate the following {output}"
#    shell:
#        "bwa mem {input} | samtools view -Sb - > {output}"

rule samtools_sort:
    input:
        join(OUTPUT_DIR, "mapped_reads/{sample}.bam")
    output:
        join(OUTPUT_DIR, "sorted_reads/{sample}.bam")
    log:
        "logs/samtools_sort/{sample}.log"
    message:
        "executing SAMtools on the following {input} to generate the following {output}"
    benchmark:
        "benchmarks/{sample}.samtools_sort.benchmark.txt"
    threads: 32
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} --threads {threads} "
        "-O bam {input} > {output}"

rule samtools_index:
    input:
        join(OUTPUT_DIR, "sorted_reads/{sample}.bam")
    output:
        join(OUTPUT_DIR, "sorted_reads/{sample}.bam.bai")
    log:
        "logs/samtools_index/{sample}.log"
    message:
        "executing SAMtools on the following {input} to generate the following {output}"
    shell:
        "samtools index {input}"

rule bcftools_call:
    input:
        fa=join(INPUT_DIR, "genome.fa"),
        bam=expand(join(OUTPUT_DIR, "sorted_reads/{sample}.bam"), sample=config["samples"]),
#        bai=expand(join(OUTPUT_DIR, "sorted_reads/{sample}.bam.bai"), sample=config["samples"])
    output:
        join(OUTPUT_DIR, "calls/all.vcf")
    log:
        "logs/bcftools_call/bcftools.log"
    message:
        "executing SAMtools and executing bcftools to call variants on the following {input} to generate {output}"
    benchmark:
        "benchmarks/bcftools_call.benchmark.txt"
    threads: 32
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv --threads {threads} - > {output}"
