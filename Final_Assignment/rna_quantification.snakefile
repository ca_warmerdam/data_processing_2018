# Get executable for homer
makeTagDirectoryExecutable = config["setup"]["homer"]["executable"]["makeTagDirectoryExecutable"]
analyzeRepeatsExecutable = config["setup"]["homer"]["executable"]["analyzeRepeatsExecutable"]
homerDirectory = config["setup"]["homer"]["directory"]

rule make_tag_directory_homer:
    '''Making tag directories from sam files'''
    input:
        lambda wildcards: alignment_output[wildcards.aligner](wildcards.type)
    output:
        done = join(OUTPUT_DIR, "{type}", "{aligner}", "RNA_quantification", "homer", "tagdir.done")
    params:
        tagdir = join(OUTPUT_DIR, "{type}", "{aligner}", "RNA_quantification", "homer", "HOMERTEMPTAGDIR")
    message:
        "Making tag directory"
    log:
        tagdir = join(OUTPUT_DIR, "logs", "rna_quantification", "homer_tagdir.{type}_{aligner}.error.log"),
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "rna_quantification", "homer_tagdir.{type}_{aligner}.benchmark.txt")
    run:
        # Change working directory for homer to function properly
        old_cwdir = os.getcwd()
        os.chdir(join(homerDirectory, "bin"))
        sam_files = " ".join([join(old_cwdir, file) for file in input])
        # Edit paths to work with changed working directory
        tagdir = join(old_cwdir, params.tagdir)
        log_tagdir = join(old_cwdir, log.tagdir)
        out = join(old_cwdir, output.done)
        shell(join(old_cwdir, "{makeTagDirectoryExecutable}") + " {tagdir} {sam_files} 2> {log_tagdir} && touch {out}")
        os.chdir(old_cwdir)

rule analyze_repeats_homer:
    '''Executes homer to analyse reads'''
    input:
        join(OUTPUT_DIR, "{type}", "{aligner}", "RNA_quantification", "homer", "tagdir.done")
    output:
        join(OUTPUT_DIR, "{type}", "{aligner}", "RNA_quantification", "homer", "quantification.txt")
    params:
        tagdir = join(OUTPUT_DIR, "{type}", "{aligner}", "RNA_quantification", "homer", "HOMERTEMPTAGDIR")
    message:
        "Analysing sam files with homer"
    log:
        analyse = join(OUTPUT_DIR, "logs", "rna_quantification", "homer.{type}_{aligner}.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "rna_quantification", "homer.{type}_{aligner}.benchmark.txt")
    run:
        # Change working directory for homer to function properly
        old_cwdir = os.getcwd()
        os.chdir(join(homerDirectory, "bin"))
        # Edit paths to work with changed working directory
        tagdir = join(old_cwdir, params.tagdir)
        log_analyse = join(old_cwdir, log.analyse)
        shell(join(old_cwdir, "{analyzeRepeatsExecutable}") + " rna mm10 -d {tagdir} -maxdiv 1.0 -mindiv 0 -minLength 0  -minLengthP 0 -maxLengthP 100 -count 3utr -strand both -pc 0 -condenseGenes -raw > " + join(old_cwdir, "{output}") + " 2> {log_analyse}")
        os.chdir(old_cwdir)
