# Get executable for hisat2
hisat2Executable = config["setup"]["hisat2"]["executable"]
# Get index for hisat2
hisat2Index = config["setup"]["hisat2"]["index"]
# Get executable for aligner star
starExecutable = config["setup"]["star"]["executable"]
# Get index for aligner star
starIndex = config["setup"]["star"]["index"]
# Get executable for bwa aligner
bwaExecutable = config["setup"]["bwa"]["executable"]
# Get index for bwa aligner
bwaIndex = config["setup"]["bwa"]["index"]

def get_input_path(type):
    '''Gets input path for given input type. can be RNA or scRNA'''
    return INPUT_FILES["{}_seq_path".format(type)]

def format_input(path, sample):
    '''Formats a basepath and a sample id to represent an input file'''
    return join(path, "{}.fastq".format(sample))

def get_input_files(type):
    '''Gets input files for a given input type. can be RNA or scRNA'''
    path = get_input_path(type)
    return [format_input(path, sample.split('.')[0]) for sample in listdir(path)]

def get_alignment_output(aligner, fileformat, type):
    '''Builds alignment output '''
    input_path = get_input_path(type)
    # obtain filenames for a given input type
    filenames = [s.split('.')[0] for s in listdir(input_path)]
    basepath = join(OUTPUT_DIR, "{type}", aligner, "Alignment", aligner, fileformat)
    return [basepath.format(type=type, sample=sample) for sample in filenames]

rule hisat2:
    '''Aligns reads using hisat2'''
    input:
        samples = lambda wildcards: get_input_files(wildcards.type),
        done = join(OUTPUT_DIR, "hisat2_genome.done")
    output:
        join(OUTPUT_DIR, "{type}", "hisat2", "Alignment", "hisat2", "output.sam")
    params:
        index = hisat2Index
    message:
        "Aligning reads using hisat2"
    log:
        join(OUTPUT_DIR, "logs", "aligning", "hisat2.{type}.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "aligning", "hisat2.{type}.benchmark.txt")
    threads:
        16
    run:
        input_reads = ",".join(input.samples)
        shell("{hisat2Executable} -p {threads} -x {params.index} -U {input_reads} --trim5 0"
        +" --trim3 0 --n-ceil L,0,0.15 --pen-cansplice 0"
        +" --pen-noncansplice 12 --pen-canintronlen G,-8,1"
        +" --pen-noncanintronlen G,-8,1"
        +" --min-intronlen 20 --max-intronlen 500000"
        +" --mp 6,2 --sp 2,1 --np 1 --rdg 5,3 --rfg 5,3"
        +" --score-min L,0.0,-0.2 -S {output} 2> {log}")

rule star:
    '''Aligns reads using star'''
    input:
        samples = lambda wildcards: format_input(get_input_path(wildcards.type), wildcards.sample),
        done = join(OUTPUT_DIR, "star_genome.done")
    output:
        join(OUTPUT_DIR, "{type}", "star", "Alignment", "star", "{sample}Aligned.out.sam")
    params:
        prefix = join(OUTPUT_DIR, "{type}", "star", "Alignment", "star", "{sample}"),
        index = starIndex
    message:
        "Aligning reads using star"
    log:
        join(OUTPUT_DIR, "logs", "aligning", "star.{type}_{sample}.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "aligning", "star.{type}_{sample}.benchmark.txt")
    threads:
        16
    run:
        shell("{starExecutable} --runThreadN {threads} --genomeDir {params.index}"\
        + " --readFilesIn {input.samples} --outFileNamePrefix {params.prefix}"\
        + " --outFilterMultimapNmax 20"\
        + " --alignSJoverhangMin 8"\
        + " --alignSJDBoverhangMin 1"\
        + " --outFilterMismatchNmax 999"\
        + " --outFilterMismatchNoverReadLmax 0.04"\
        + " --alignIntronMin 20 --alignIntronMax 1000000"\
        + " --alignMatesGapMax 1000000 2> {log}")

rule bwa:
    '''Aligns reads using bwa'''
    input:
        samples = lambda wildcards: format_input(get_input_path(wildcards.type), wildcards.sample),
        done = join(OUTPUT_DIR, "bwa_genome.done")
    output:
        join(OUTPUT_DIR, "{type}", "bwa", "Alignment", "bwa", "{sample}.sam")
    params:
        index = bwaIndex
    message:
        "Aligning reads using bwa"
    log:
        join(OUTPUT_DIR, "logs", "aligning", "bwa.{type}_{sample}.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "aligning", "bwa.{type}_{sample}.benchmark.txt")
    threads:
        16
    run:
        shell("{bwaExecutable} mem -t {threads} -k 19 -w 100 -d 100"
                           + " -r 1.5 -c 500 -D 0.5 -m 50 -W 0"
                           + " -A 1 -B 4 -O 6 -E 1 -L 5"
                           + " -U 17 {params.index} {input.samples} > {output} 2> {log}")

alignment_output = {"hisat2": lambda type: rules.hisat2.output,
                    "star": lambda type: get_alignment_output("star", "{sample}Aligned.out.sam", type),
                    "bwa": lambda type: get_alignment_output("bwa", "{sample}.sam", type)}

