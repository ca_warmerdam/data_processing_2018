# Get executable for hisat2
hisat2IndexExecutable = config["setup"]["hisat2"]["index_executable"]
# Get index for hisat2
hisat2Index = config["setup"]["hisat2"]["index"]
# Get executable for aligner star
starIndexExecutable = config["setup"]["star"]["index_executable"]
# Get index for aligner star
starIndex = config["setup"]["star"]["index"]
# Get executable for bwa aligner
bwaIndexExecutable = config["setup"]["bwa"]["index_executable"]
# Get index for bwa aligner
bwaIndex = config["setup"]["bwa"]["index"]

# Get genome fasta files directory
INPUT_DIR = config["genome_files_dir"]

def genome_files(wildcards):
    '''Collects all genome fasta files'''
    return [join(INPUT_DIR, f) for f in listdir(INPUT_DIR) if f.endswith(".fa")]

rule index_genome:
    input:
        expand(join(OUTPUT_DIR, "{aligner}_genome.done"), aligner=config["operate"]["alignment"])

rule create_hisat2_index:
    '''Indexes a genome for hisat2 and saves the hisat2_genome.done flag file'''
    input:
        join(OUTPUT_DIR, "genome.fa")
    output:
        join(OUTPUT_DIR, "hisat2_genome.done")
    params:
        ht2_base = hisat2Index
    message:
        "Indexing a genome for hisat2 and saving the hisat2_genome.done flag file"
    log:
        join(OUTPUT_DIR, "logs", "indexing", "hisat2.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "indexing", "hisat2.benchmark.txt")
    shell:
        "mkdir -p {params.ht2_base} && {hisat2IndexExecutable} -f {input} {params.ht2_base} 2> {log} && touch {output}"

rule create_bwa_index:
    '''Indexes a genome for bwa and saves the bwa_genome.done flag file'''
    input:
        join(OUTPUT_DIR, "genome.fa")
    output:
        join(OUTPUT_DIR, "bwa_genome.done")
    params:
        prefix = bwaIndex
    message:
        "Indexing a genome for bwa and saving the bwa_genome.done flag file"
    log:
        join(OUTPUT_DIR, "logs", "indexing", "bwa.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "indexing", "bwa.benchmark.txt")
    shell:
        "mkdir -p {params.prefix} && {bwaIndexExecutable} index -p {params.prefix} {input} 2> {log} && touch {output}"

rule create_star_index:
    '''Indexes a genome for star and saves the star_genome.done flag file'''
    input:
        genome_files
    output:
        join(OUTPUT_DIR, "star_genome.done")
    params:
        prefix = starIndex
    message:
        "Indexing a genome for star and saving the star_genome.done flag file"
    log:
        join(OUTPUT_DIR, "logs", "indexing", "star.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "indexing", "star.benchmark.txt")
    threads:
        16
    run:
        shell("mkdir -p {params.prefix} && {starIndexExecutable} --runMode genomeGenerate"
        +" --runThreadN {threads} --genomeDir {params.prefix}"
        +" --genomeSAindexNbases 10 --genomeChrBinNbits 14 --genomeFastaFiles "
        +" {input} 2> {log} && touch {output}")

rule concat_genome:
    '''Combines the genome fasta files into a single fasta file'''
    input:
        genome_files
    output:
        join(OUTPUT_DIR, "genome.fa")
    message:
        "Concatinating the genome fasta files into a single fasta file"
    log:
        join(OUTPUT_DIR, "logs", "genome_concatination", "error.log")
    shell:
        "cat {input} > {output} 2> {log}"
