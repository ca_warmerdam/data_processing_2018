# Get executable for sorting pipelines and generating boxplots
sortPipelinesExecutable = config["setup"]["sort_pipelines"]["executable"]

def analysis_output():
    '''returns the output of an analysis'''
    return join(SUMMARIES_OUTPUT_DIR, "{aligner_one}_{aligner_two}", "output_summary.csv")

rule sort_pipelines:
    '''sorts pipelines'''
    input:
        expand(analysis_output(), aligner_one=ALIGNERS, aligner_two=ALIGNERS)
    output:
        join(SUMMARIES_OUTPUT_DIR, "output_boxplots.svg")
    message:
        "Sorting pipelines"
    log:
        join(OUTPUT_DIR, "logs", "sorting_pipelines", "sort_pipelines.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "sorting_pipelines", "sort_pipelines.benchmark.txt")
    shell:
        "{sortPipelinesExecutable} {SUMMARIES_OUTPUT_DIR}"
