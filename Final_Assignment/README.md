# Final assignment - Single cell / multi cell mapper comparison
In this project multiple aligners are compared within a pipeline to determine what combination(s) 
of aligners are the best when comparing single-cell and multi-cell sequencing data. 
Three aligners are compared: Hisat2, STAR and bwa. 
These all have their own way of aligning with slightly different indexing and memory management.

## diagram
![alt text](dag.png "drawing")

- The input data is located here: `/data/storix2/scsa/Downloaded_EBI_fastqs/GSE79819`
- The output data is located here: `/data/storix2/scsa/Processed_data/Robert`

Robert Warmerdam
346462
BFV3