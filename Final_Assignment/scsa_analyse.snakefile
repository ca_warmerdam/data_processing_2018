# Get executable for analyzing pipelines and generating boxplots
scsaAnalyseExecutable = config["setup"]["scsa_analyse"]["executable"]

rule analyse_pipeline:
    '''Compares to pipelines'''
    input:
        sc_homertxt = join(OUTPUT_DIR, "scRNA", "{aligner_one}", "RNA_quantification", "homer", "quantification.txt"),
        mc_homertxt = join(OUTPUT_DIR, "RNA", "{aligner_two}", "RNA_quantification", "homer", "quantification.txt")
    output:
        join(SUMMARIES_OUTPUT_DIR, "{aligner_one}_{aligner_two}", "output_summary.csv")
    params:
        join(SUMMARIES_OUTPUT_DIR, "{aligner_one}_{aligner_two}")
    message:
        "Comparing aligners using scsa_analyse R script"
    log:
        join(OUTPUT_DIR, "logs", "analyse_pipeline", "analyse.{aligner_one}_{aligner_two}.error.log")
    benchmark:
        join(OUTPUT_DIR, "benchmarks", "analyse_pipeline", "analyse.{aligner_one}_{aligner_two}.benchmark.txt")
    shell:
        "{scsaAnalyseExecutable} {input.sc_homertxt},{input.mc_homertxt} {params} 8 scRNA,RNA"
