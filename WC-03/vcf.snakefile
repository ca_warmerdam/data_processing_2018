INPUT_DIR = "data/"
OUTPUT_DIR = "out/"
FASTQ_DIR = "samples/"

rule bwa_mem:
    input:
        join(INPUT_DIR, "genome.fa"),
        join(INPUT_DIR, FASTQ_DIR, "{sample}.fastq")
    output:
        join(OUTPUT_DIR, "mapped_reads/{sample}.bam")
    message: "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"

rule samtools_sort:
    input:
        join(OUTPUT_DIR, "mapped_reads/{sample}.bam")
    output:
        join(OUTPUT_DIR, "sorted_reads/{sample}.bam")
    message:
        "executing SAMtools on the following {input} to generate the following {output}"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"

rule samtools_index:
    input:
        join(OUTPUT_DIR, "sorted_reads/{sample}.bam")
    output:
        join(OUTPUT_DIR, "sorted_reads/{sample}.bam.bai")
    message:
        "executing SAMtools on the following {input} to generate the following {output}"
    shell:
        "samtools index {input}"

rule bcftools_call:
    input:
        fa=join(INPUT_DIR, "genome.fa"),
        bam=expand(join(OUTPUT_DIR, "sorted_reads/{sample}.bam"), sample=config["samples"]),
#        bai=expand(join(OUTPUT_DIR, "sorted_reads/{sample}.bam.bai"), sample=config["samples"])
    output:
        join(OUTPUT_DIR, "calls/all.vcf")
    message:
        "executing SAMtools and executing bcftools to call variants on the following {input} to generate {output}"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"
